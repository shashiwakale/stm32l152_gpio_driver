#ifndef __GPIO_H
#define __GPIO_H


#include "stm32l1xx.h"                  // Device header

#include <stdint.h>


#define SET_AS_INPUT 						((uint32_t)0x00)		
#define SET_AS_OUTPUT 					((uint32_t)0x01)
#define SET_AS_AF								((uint32_t)0x02)
#define SET_AS_ANALOG						((uint32_t)0x03)

#define OUTPUT_TYPE_PUSHPULL	 	((uint32_t)0x00)
#define OUTPUT_TYPE_OPEN_DRAIN 	((uint32_t)0x01)

#define GPIO_LOW_SPEED					((uint32_t)0x00)
#define GPIO_MEDIUM_SPEED				((uint32_t)0x01)
#define GPIO_HIGH_SPEED 				((uint32_t)0x02)
#define GPIO_VERY_HIGH_SPEED 		((uint32_t)0x03)

#define GPIO_NOPULLUP						((uint32_t)0x00)
#define GPIO_PULLUP							((uint32_t)0x01)
#define GPIO_PULLDOWN						((uint32_t)0x02)

#define GPIO_PIN_SET						1
#define GPIO_PIN_RESET					0

#define GPIOA_ENABLE_CLCK 			( RCC->AHBENR |= (1<<0) )
#define GPIOB_ENABLE_CLCK 			( RCC->AHBENR |= (1<<1) )
#define GPIOC_ENABLE_CLCK 			( RCC->AHBENR |= (1<<2) )
#define GPIOD_ENABLE_CLCK 			( RCC->AHBENR |= (1<<3) )
#define GPIOE_ENABLE_CLCK 			( RCC->AHBENR |= (1<<4) )

#define PORTA										( (uint32_t)0x01 )
#define PORTB										( (uint32_t)0x02 )
#define PORTC										( (uint32_t)0x03 )
#define PORTD										( (uint32_t)0x04 )
#define PORTE										( (uint32_t)0x05 )


#define	PIN_0  		((uint16_t) 0x0001U)
#define	PIN_1  		((uint16_t) 0x0002U)
#define	PIN_2 		((uint16_t) 0x0004U)
#define	PIN_3  		((uint16_t) 0x0008U)
#define	PIN_4  		((uint16_t) 0x0010U)
#define	PIN_5  		((uint16_t) 0x0020U)
#define	PIN_6  		((uint16_t) 0x0040U)
#define	PIN_7  		((uint16_t) 0x0080U)
#define	PIN_8  		((uint16_t) 0x0100U)
#define	PIN_9  		((uint16_t) 0x0200U)
#define	PIN_10  	((uint16_t) 0x0400U)
#define	PIN_11  	((uint16_t) 0x0800U)
#define	PIN_12 	  ((uint16_t) 0x1000U)
#define	PIN_13	  ((uint16_t) 0x2000U)
#define	PIN_14	  ((uint16_t) 0x4000U)
#define	PIN_15	  ((uint16_t) 0x8000U)

typedef struct g_sGpioConfig{
	uint32_t Pin;
	uint32_t Mode;
	uint32_t Otype;
	uint32_t Speed;
	uint32_t PuPd;
}g_sGpioConfig;


/* Functions */
void FGpioInit(GPIO_TypeDef *GPIOx, g_sGpioConfig *spGpioConfig);
void FGpioWrite(GPIO_TypeDef *GPIOx, uint32_t p_u32Pin, uint8_t p_u8State);


uint16_t GPIO_Read (GPIO_TypeDef *GPIOx, uint32_t pin);

#endif
