#include "stm32l1xx.h"                  // Device header
#include "GPIO.h"

void InitClock(void);			// Clock Initilize Function
void InitGPIO (void);			// GPIO Initilize Function

g_sGpioConfig myGPIO;				// GPIO Config Structure

int main(){
	InitClock();				// Initilize clock
	InitGPIO();					// Initilize GPIO

	while(1)
	{
		FGpioWrite(GPIOB,PIN_7,GPIO_PIN_SET);
		for(int i=100000;i>0;i--);		// Delay
		FGpioWrite(GPIOB,PIN_7,GPIO_PIN_RESET);
		for(int i=100000;i>0;i--);		// Delay
	}
}

void InitClock(void){
	RCC->CR |= 0x01; 						// Enable Internal HSI
	RCC->CFGR |= (1<<7)|(1<<4); // Divide Clock by 2
	RCC->CFGR |= 0x01; 					// Switch Sys Clock to HSI
}

void InitGPIO(void){
	
	GPIOB_ENABLE_CLCK;										// Enable GPIO B Clock
	
	myGPIO.Pin = PIN_7|PIN_6;							// Select Pin 7 and Pin 6
	myGPIO.Mode = SET_AS_OUTPUT;					// Set Them as o/p
	myGPIO.PuPd = GPIO_NOPULLUP;					// Set No Pull-up
	myGPIO.Otype = OUTPUT_TYPE_PUSHPULL;	// Set o/p type to push-pull
	myGPIO.Speed = GPIO_HIGH_SPEED;				// Set speed of GPIO i.e. High speed
	
	FGpioInit(GPIOB,&myGPIO);							// Initilize GPIO B
	
}

	/*
	// Enable AHB Peripheral Clock
	RCC->AHBENR |= (1<<1);		// Enable GPIOB Clock
	RCC->AHBENR |= 0x01;			// Enable GPIOA Clock
	// Configure Output Bits
	GPIOB->MODER |= (1<<14)|(1<<12);
	GPIOB->OTYPER|= (0x00<<7);
	GPIOB->OSPEEDR |= ( (1<<14)|(1<<15)|(1<<13)|(1<<12) );	
	
	// Configure Input Bits
	GPIOA->OSPEEDR |= 0x03;
	GPIOA->PUPDR |= 0x01;
	*/

/*		
		//GPIOB->ODR = (1<<7);
		GPIOB->BSRR = (1<<7);					// Set High Bit
		for(int i=100000;i>0;i--);		// Delay
		//GPIOB->ODR ^= (1<<7);				// Set Low Bit
		GPIOB->BSRR = (1<<23);				// Set low bit
		for(int i=100000;i>0;i--);		// Delay
		bit = ((GPIOA->IDR)&(0x01));	// Scan I/P Bit
		if(bit)
			GPIOB->BSRR |= (1<<6);			// Set LED HIGH
		else
			GPIOB->BSRR |= (1<<22);			// Set LED Low
		*/
		
