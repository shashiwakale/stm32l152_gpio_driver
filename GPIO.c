#include "GPIO.h"

void FGpioInit(GPIO_TypeDef *GPIOx, g_sGpioConfig *spGpioConfig){
	
	uint16_t l_u16BitPosition = 0x00;
	uint16_t l_u16ValidPin = 0x00;
	
	while((spGpioConfig->Pin >> l_u16BitPosition) != 0)
	{
		l_u16ValidPin = ((spGpioConfig->Pin>>l_u16BitPosition) & 0x01);
		if(l_u16ValidPin){
		// Set GPIO Mode
		GPIOx->MODER |= (	(	spGpioConfig->Mode << ((l_u16BitPosition)*2 ))	);
		// Configure Speed
		GPIOx->OSPEEDR |= (	(	spGpioConfig->Speed << ((l_u16BitPosition)*2) ) );
		// Configure Pull/Pull Down
		GPIOx->PUPDR |= (	(	spGpioConfig->PuPd << ((l_u16BitPosition)*2) ) );
		// Output Type
		GPIOx->OTYPER |= (spGpioConfig->Otype << l_u16BitPosition);
		}
		l_u16BitPosition++;
	}
}

void FGpioWrite(GPIO_TypeDef *GPIOx, uint32_t p_u32Pin, uint8_t p_u8State){
	
	if(p_u8State==GPIO_PIN_SET)
		GPIOx->BSRR |= (p_u32Pin);
	else	
		GPIOx->BSRR |= (p_u32Pin << 16);

}

uint16_t GPIO_Read (GPIO_TypeDef *GPIOx, uint32_t pin){
	return ( GPIOx->IDR &= pin);
}
